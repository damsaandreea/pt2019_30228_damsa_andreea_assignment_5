
import java.nio.file.Files;
import java.util.stream.Collectors;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.stream.Stream;

import org.omg.Messaging.SyncScopeHelper;

public class ReadClass {
	ArrayList<MonitoredData> activities=new ArrayList<MonitoredData>();
	private long countDays;
	public void read() {
		String fileName="Activities.txt";
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
	       stream.forEach(this::compute);
	        //count=stream.count();
	       }catch(Exception e) {
		System.out.println(e.getMessage());
	}
	}
	public void compute(String s) {
		String[] separated=s.split("\t\t");
		DateFormat form=new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
		try {
		DateTimeFormatter formatter=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
		LocalDateTime dates=LocalDateTime.parse(separated[0], formatter);
		LocalDateTime datee=LocalDateTime.parse(separated[1], formatter);
		MonitoredData md=new MonitoredData(dates, datee, separated[2]);
		activities.add(md);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public void countDays() {
		countDays=activities.stream()
				.map(d->d.getDay(d.getStart_time()))
				.distinct().count();
	}
	public HashMap<String ,Integer>  eachActivity() {
		List<String> lista=activities.stream().map(d->d.getActivity_label()).distinct().collect(Collectors.toList());
		HashMap<String ,Integer> hm=new HashMap<String, Integer>();
		for (String activitate : lista) {
			long aparitii=activities.stream().map(d->d.getActivity_label()).filter(d->d.equals(activitate)).count();
			Integer i=new Integer((int) aparitii);
			hm.put(activitate, i);
		}
		return hm;
	}
	public HashMap<String ,HashMap<String, Integer>> eachActivityeachDay() {
		HashMap<String ,HashMap<String, Integer>> hm=new HashMap<String, HashMap<String, Integer>>();
		//mapez la fiecare zi alt hash map
		List<String> zile=activities.stream().map(d->d.getDay(d.getStart_time())).distinct().collect(Collectors.toList());
		for (String zi: zile) {
			HashMap<String, Integer> subhm=new HashMap<String, Integer>();
			List<MonitoredData> activitati=activities.stream().filter(d->d.getDay(d.getStart_time()).equals(zi)).collect(Collectors.toList());
			for (MonitoredData act: activitati) {
				long numar=activities.stream().filter(d-> d.getDay(d.getStart_time()).equals(zi) && d.getActivity_label().equals(act.getActivity_label())).count();
				Integer i=new Integer((int) numar);
				subhm.put(act.getActivity_label(), i);
			}
			hm.put(zi, subhm);
		}
		return hm;
	}
	public HashMap<MonitoredData, Long> durationForEachLine() {
		HashMap<MonitoredData, Long> dur=new HashMap<MonitoredData,Long>();
		List<Long> durate=activities.stream().map(d-> Duration.between(d.getStart_time(), d.getEnd_time()).toMinutes()).collect(Collectors.toList());
		for (int i=0; i<activities.size(); i++) {
			MonitoredData m=activities.get(i);
			Long l=durate.get(i);
			dur.put(m, l);
		}
			return dur;
	}
	public HashMap<String, Long> eachActivityEntireDuration() {
		HashMap<String, Long> maparea=new HashMap<String, Long>();
		HashMap<MonitoredData,Long> dur=this.durationForEachLine();
		List<String> numeActivitati=activities.stream().map(d->d.getActivity_label()).distinct().collect(Collectors.toList());
		//pot sa folosesc get de key
		for (String nume: numeActivitati) {
		 Set<MonitoredData> unFel=dur.keySet().stream().filter(d-> d.getActivity_label().equals(nume)).collect(Collectors.toSet());
		 long total=0;
		 for (MonitoredData md: unFel) {
			 total+=dur.get(md);
		 }
		 maparea.put(nume, total);
		}
		return maparea;
	}
	public boolean verificare(String name) {
		HashMap<MonitoredData, Long> dur=this.durationForEachLine();
		long numarTotal=dur.keySet().stream().filter(d-> d.getActivity_label().trim().equals(name)).count();
		long numarDurataSub= dur.keySet().stream().filter(d-> d.getActivity_label().trim().equals(name)).filter(d->dur.get(d)<5).count();
		float raport=(float)numarDurataSub/(float)numarTotal;
		//System.out.println("total si sub "+ numarTotal+" "+numarDurataSub);
		//System.out.println("Raportul pt "+name+" este "+raport);
		if (raport>=0.9)
			return false;
		return true;
	}
	public List<String> ninetyPercent() {
		// activitati ale carori iregistrari 90% cu durata sub 5 min
		List<String> ramase=activities.stream().map(d->d.getActivity_label().trim()).distinct().filter(d-> this.verificare(d)==true).collect(Collectors.toList());
		return ramase;
	}
	
	public void consola() {
		/*for (MonitoredData md:activities) {
			System.out.println(md.toString());
		}*/
		//System.out.println();
	System.out.println("Numar total de zile inregistrari:"+countDays);
	System.out.println();
	System.out.println("Fiecare activitate de cate ori a aparut:");
	Iterator<Entry<String, Integer>> it=eachActivity().entrySet().iterator();
	while (it.hasNext()) {
		Entry<String, Integer> pair= it.next();
		System.out.println(pair.getKey().trim()+ " apare de "+ pair.getValue()+ " ori");		
	}
	System.out.println();
	System.out.println("De cate ori apare fiecare activitate in fiecare zi:");
	HashMap<String, HashMap<String, Integer>> hm=eachActivityeachDay();
	Iterator<Entry<String, HashMap<String, Integer>>> it1 =hm.entrySet().iterator();
	while (it1.hasNext()) {
		Entry<String, HashMap<String, Integer>> pair1=it1.next();
		System.out.println("Pentru ziua "+ pair1.getKey()+ " activitatile sunt:");
		Iterator<Entry<String, Integer>> it2=pair1.getValue().entrySet().iterator();
		while (it2.hasNext()) {
			Entry<String, Integer> pair2=it2.next();
			System.out.println("\t"+pair2.getKey().trim()+" apare de "+pair2.getValue()+" ori");
		}
		System.out.println();
	}
	System.out.println();
	System.out.println("Durata in minute pentru fiecare inregistrare din fisier:");
	HashMap<MonitoredData, Long> hm1=durationForEachLine();
	Iterator<Entry<MonitoredData, Long>> it3=hm1.entrySet().iterator();
	int index=1;
	while (it3.hasNext()) {
		Entry<MonitoredData, Long> pair3=it3.next();
		System.out.println("Activitatea "+pair3.getKey().getActivity_label().trim()+ " de la linia "+ (index++) +" a durat "+pair3.getValue()+ " minute");
	}
	System.out.println();
	System.out.println("Durata totala a activitatilor:");
	HashMap<String, Long> dur=this.eachActivityEntireDuration();
	for (Map.Entry<String, Long> entry : dur.entrySet()) {
	    String key = entry.getKey();
	    Long value = entry.getValue();
	    System.out.println("Activitatea "+key.trim()+" a durat in total "+ value+" minute");
	}
	System.out.println();
	System.out.println("Activitatile nefiltrate sunt:");
	List<String> lista= this.ninetyPercent();
	for (String nume: lista) {
		System.out.println(nume);
	}
	//this.verificare("Snack");
	}
}