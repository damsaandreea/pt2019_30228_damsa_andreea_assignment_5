import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class MonitoredData {
	private LocalDateTime start_time;
	private LocalDateTime end_time;
	private String activity_label;
	public LocalDateTime getStart_time() {
		return start_time;
	}
	public void setStart_time(LocalDateTime start_time) {
		this.start_time = start_time;
	}
	public LocalDateTime getEnd_time() {
		return end_time;
	}
	public void setEnd_time(LocalDateTime end_time) {
		this.end_time = end_time;
	}
	public String getActivity_label() {
		return activity_label;
	}
	public void setActivity_label(String activity_label) {
		this.activity_label = activity_label;
	}
	public MonitoredData(LocalDateTime start_time, LocalDateTime end_time, String activity_label) {
		super();
		this.start_time = start_time;
		this.end_time = end_time;
		this.activity_label = activity_label;
	}
	public String getDay(LocalDateTime d) {
		String ymd="";
		int year = d.getYear();
		int month = d.getMonthValue();
		int day = d.getDayOfMonth();
		ymd=year+"/"+month+"/"+day;
		return ymd;
		}

	@Override
	public String toString() {
		return "MonitoredData [start_time=" + start_time + ", end_time=" + end_time + ", activity_label="
				+ activity_label + "]\n";
	}
	
}
